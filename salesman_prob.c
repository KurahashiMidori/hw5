/* salesman_prob.c */

#include<stdio.h>
#include<math.h>   // -lm
#define MAX_POINT 64
//5 8 16 64 128 512 2048

float buffer[2][MAX_POINT];

float x_to_y_kyori(float x, float y){
  float ret;
  ret = (float)sqrt((double)x*x + y*y);
  return ret;
}

int main(void){
  float buf_line1[5];
  float dis = 0, ans_dis = 0, imp_dis = 0;
  float longest1=0, longest2=0;
  int p1_1, p1_2, p2_1, p2_2;
  int i = 0;
  int j = 0;
  int line;

  FILE *fp;
  fp = fopen("input_3.csv", "r");

  // 一行目を読み込む
  fscanf(fp, "%s", buf_line1);
  printf("%s\n",buf_line1);

  // ファイルから一行読み込み、間を計算する
  while(fscanf(fp, "%f,%f", &buffer[1][i], &buffer[2][i]) != EOF){
    i++; 
  }
  line = i;
  for(i=0; i<=line; i++){
      dis = x_to_y_kyori(buffer[1][i]-buffer[1][(i+1)%line],
			     buffer[2][i]-buffer[2][(i+1)%line]);
      ans_dis += dis;
      printf("point%d : %f, total = %f\n", i, dis, ans_dis);
    if(longest1 < dis){
      longest1 = dis;
      p1_1 = i;
      p1_2 = (i+1) % line;
    }else if(longest2 < dis){
      longest2 = dis;
      p2_1 = i;
      p2_2 = (i+1) % line;
    }
  }
  printf("the longest1 = %f  %d, %d\n", longest1, p1_1, p1_2);
  printf("the longest2 = %f  %d, %d\n", longest2, p2_1, p2_2);

  printf("ANS : %f\n", ans_dis);

  /* 長い辺同士をスイッチ */
  imp_dis = ans_dis - longest1 - longest2;
  imp_dis = imp_dis + x_to_y_kyori(buffer[1][p1_1]-buffer[1][p2_1],
				   buffer[2][p1_1]-buffer[2][p2_1]);
  imp_dis = imp_dis + x_to_y_kyori(buffer[1][p1_2]-buffer[1][p2_2],
				   buffer[2][p1_2]-buffer[2][p2_2]);
  printf("IMPROVED ANS : %f\n", imp_dis);
  
  fclose(fp);

}
