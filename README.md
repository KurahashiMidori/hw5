# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
：巡回セールスマン問題について、複数の点の座標が与えられたとき、全てをまわる経路を考えた。
salesman_greedy.cは、現在見ている点から一番近い点へ行く方法。
salesman_prob.cは、入力された順にたどっていく方法。

* Version
：現在、salesman_prob.cは、１番長い辺と２番目に長い辺を見て、うまく通る順を入れ替えることによって経路を短くしようと試みているが、未完成である。

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact