/* salesman_prob.c */

#include<stdio.h>
#include<math.h>   // -lm
#define MAX_POINT 8  // 予め入力の点の数がわかっていないといけない
//5 8 16 64 128 512 2048
#define DIS_MAX 1000000000

float x_to_y_kyori(float x, float y){
  float ret;
  ret = (float)sqrt((double)x*x + y*y);
  return ret;
}

int main(void){
  float buf_line1[5];
  int ans[MAX_POINT];
  float dis = DIS_MAX;
  float tmp_dis=0, ans_dis=0;
  int shiten, syuten;
  int i=0, j=0, k=0;
  char output[5];
  float buffer[2][MAX_POINT];


  FILE *fp, *fp_out;
  fp = fopen("input_2.csv", "r");

  // 数字でない一行目を読み込む
  fscanf(fp, "%s", buf_line1);
  printf("%s\n",buf_line1);

  // ファイルからすべて読み込み、保存
  while(fscanf(fp, "%f,%f", &buffer[1][i], &buffer[2][i]) != EOF){
    i++; 
  }
  printf("Done!\n");
  ans[0] = 0;
  shiten = 0;
  for(i=0; i<MAX_POINT; i++){  // shiten に対し
    if(i == MAX_POINT-1){  // もし最後の点だったら始点につないで終了
      syuten = 0;
      dis = x_to_y_kyori(buffer[1][shiten]-buffer[1][j],
			 buffer[2][shiten]-buffer[2][j]);
      printf("  -> conect (%d, %d) dis = %f (last)\n", shiten, syuten, dis);
      ans_dis += dis;
      break;
    }else{
      for(j=0; j<MAX_POINT; j++){ // 点 j を接続するか検証
	// printf("try (%d, %d)\n", i, j);
	for(k=0; k<i; k++){ // 点 j がすでに使ってある点か確認
	  if(ans[k] == j){
	    break;
	  }
	}
	if((k == i) && (j != shiten)){         // 使ってなかったら
	  tmp_dis = x_to_y_kyori(buffer[1][shiten]-buffer[1][j],
				 buffer[2][shiten]-buffer[2][j]);
	  if(dis > tmp_dis){
	    dis = tmp_dis;
	    syuten = j;
	    printf("dis (%d,%d) : %f\n", shiten, j, dis);
	  }
	}
      }  // このループ文終了時、次に接続する点が決まっている
      printf("  -> connect (%d, %d) dis = %f\n", shiten, syuten, dis);
      ans[i+1] = syuten;
      shiten = syuten;
      ans_dis += dis;
      dis = DIS_MAX;
    }
  }
  printf("ANS : %f\n", ans_dis);
  fclose(fp);
  /* 確認用
  for(i=0; i<MAX_POINT; i++){
    printf("%d\n", ans[i]);
  }
  */
  fp_out = fopen("solution_2.csv", "w");
  fputs("index\n", fp_out);
  for(i=0; i<MAX_POINT; i++){
    sprintf(output, "%d\n", ans[i]);
    fputs(output, fp_out);
  }
}
